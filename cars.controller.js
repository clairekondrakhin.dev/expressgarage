const express = require("express");
const router = express.Router();

// Importe le module mysql2 pour interagir avec des bases de données MySQL.
const connection = require("./conf/db");
const { validatecarData } = require("./validator.middleware");

//POST
// Route POST pour ajouter une nouvelle voiture dans la base de données
router.post("/",validatecarData, (req, res) => {
  const { brand, model } = req.body;
  // Exécution d'une requête SQL pour insérer une nouvelle voiture
  connection.execute(
    "INSERT INTO car (brand, model) VALUES (?, ?)",
    [brand, model],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'ajout de la voiture");
      } else {
        // Confirmation de l'ajout de la voiture avec l'ID de la voiture inséré
        res
          .status(201)
          .send(`La voiture ajoutée avec l'ID ${results.insertId}`);
      }
    }
  );
});

// Update (PUT)
router.put("/:id",validatecarData, (req, res) => {
  const { brand, model } = req.body;
  const id = req.params.id;
  // Exécution d'une requête SQL pour mettre à jour les informations de la voiture spécifiée
  connection.execute(
    "UPDATE car SET brand = ?, model = ? WHERE car_id = ?",
    [brand, model, id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour de la voiture");
      } else {
        // Confirmation de la mise à jour de la voiture
        res.send(`Voiture mise à jour.`);
      }
    }
  );
});

//DELETE
// Route DELETE pour supprimer une voiture par son ID
router.delete("/:id", (req, res) => {
  const id = req.params.id;
  // Exécution d'une requête SQL pour supprimer une voiture spécifiée
  connection.execute(
    "DELETE FROM car WHERE car_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la suppression de la voiture");
      } else {
        // Confirmation de la suppression de la voiture
        res.send(`voiture supprimée.`);
      }
    }
  );
});

// définition d'une route GET sur l'URL racine ('/')
router.get("/",(req, res) => {
  // Exécution d'une requête SQL pour sélectionner tous les livres
  connection.query("SELECT * FROM car", (err, results) => {
    // Gestion d'erreurs éventuelles lors de l'exécution de la requête
    if (err) {
      res.status(500).send("Erreur lors de la récupération de garage express");
    } else {
      // Envoi des résultats sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});

// Route GET pour récupérer de la voiture spécifique par sa marque en utilisant une requête préparée pour éviter les injections SQL
router.get("/:brand", (req, res) => {
  const brand = req.params.brand;
  // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
  connection.execute(
    "SELECT * FROM car WHERE brand = ?",
    [brand],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la récupération de la voiture");
      } else {
        // Envoi du résultat sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});

module.exports = router;
