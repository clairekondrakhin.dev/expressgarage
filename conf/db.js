const mysql = require("mysql2");

// Configuration et création d'un pool de connexions à la base de données MySQL
const connection = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
});


// Établissement de la connexion
connection.getConnection((err) => {
  if (err instanceof Error) {
    console.log("getConnection error:", err);
    return;
  }
});

module.exports = connection;
