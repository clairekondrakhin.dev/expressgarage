const express = require("express");
const router = express.Router();

// Importe le module mysql2 pour interagir avec des bases de données MySQL.
const connection = require("./conf/db");
const { validategarageData } = require("./validator.middleware");

//POST
// Route POST pour ajouter une nouveau garage dans la base de données
router.post("/", validategarageData,(req, res) => {
  const { name, email } = req.body;
  // Exécution d'une requête SQL pour insérer un nouveau garage
  connection.execute(
    "INSERT INTO garage (name, email) VALUES (?, ?)",
    [name, email],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'ajout du garage");
      } else {
        // Confirmation de l'ajout du garage avec l'ID du garage inséré
        res
          .status(201)
          .send(`Le garage ajouté avec l'ID ${results.insertId}`);
      }
    }
  );
});


// Update (PUT)
router.put("/:id", validategarageData, (req, res) => {
  const { name, email} = req.body;
  const id = req.params.id;
  // Exécution d'une requête SQL pour mettre à jour les informations du garage
  connection.execute(
    "UPDATE garage SET name = ?, email = ? WHERE garage_id = ?",
    [name, email, id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour du garage");
      } else {
        // Confirmation de la mise à jour du garage
        res.send(`Garage mise à jour.`);
      }
    }
  );
});

//DELETE
// Route DELETE pour supprimer un garage par son ID
router.delete("/:id",(req, res) => {
  const id = req.params.id;
  // Exécution d'une requête SQL pour supprimer une voiture spécifiée
  connection.execute(
    "DELETE FROM garage WHERE garage_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la suppression du garage");
      } else {
        // Confirmation de la suppression du garage
        res.send(`Garage supprimé.`);
      }
    }
  );
});

//get
// définition d'une route GET sur l'URL racine ('/')
router.get("/",(req, res) => {
  // Exécution d'une requête SQL pour sélectionner tous les livres
  connection.query("SELECT * FROM garage", (err, results) => {
    // Gestion d'erreurs éventuelles lors de l'exécution de la requête
    if (err) {
      res.status(500).send("Erreur lors de la récupération de garage express");
    } else {
      // Envoi des résultats sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});


module.exports = router;
