const express = require("express");
const bcrypt = require("bcrypt");
// Configuration de dotenv dans l'application
require("dotenv").config();

//configuration de CORS
const cors = require("cors");
const app = express();

//configuration
app.use(
  cors({
    origin: "http://localhost:8080",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
    optionsSuccessStatus: 204,
  })
);

// Configure l'application pour parser les requêtes avec des corps au format JSON.
app.use(express.json());

// Importe le middleware logger
const { logRequest } = require('./logger.middleware');

// Utilise le middleware pour logger les requêtes
app.use(logRequest);


// Configure l'application pour parser les requêtes avec des corps encodés en URL (utile pour les formulaires web).
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Importe le contrôleur des voitures et des garages
const carsController = require('./cars.controller');
const garageController = require('./garage.controller');
const userController = require('./user.controller');

// Utilise le contrôleur pour gérer les routes des voitures et des garages
app.use('/cars', carsController);
app.use('/garages', garageController);
app.use('/users', userController);


const port = 3000;
// Démarre le serveur Express et écoute sur le port spécifié
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});


