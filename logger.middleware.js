// logger.middleware.js
const logRequest = (req, res, next) => {
  // affiche la date, la méthode HTTP et l'URL de la requête
  console.log(
    `${new Date().toLocaleString()}: ${req.method} ${req.originalUrl}`
  );
  // passe au middleware suivant
  next();
};

module.exports = { logRequest };
