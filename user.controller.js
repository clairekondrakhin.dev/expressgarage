const express = require("express");
const router = express.Router();
const connection = require("./conf/db");
const { validateUserData } = require("./validator.middleware");
const bcrypt = require("bcrypt");


// route register
router.post("/register", validateUserData, (req, res) => {
    const { email, password } = req.body;
    // hacher le mot de passe (ici 10 correspond au nombre de "salt round")
    bcrypt.hash(password, 10, function (bcryptError, hashedPassword) {
      if (bcryptError) {
        res.status(500).json({ error: bcryptError });
      } else {
        // enregistrer le compte en base de données
        connection.execute(
          `INSERT INTO user(email, password) VALUES (?, ?)`,
          [email, hashedPassword],
          (mysqlError, result) => {
            if (mysqlError) {
              res.status(500).json({ error: mysqlError });
            } else {
              // on retourne le compte créé (mais sans le mot de passe !)
              res.status(201).json({
                id: result.insertId,
                email,
              });
            }
          }
        );
      }
    });
}

);


router.post("/login", validateUserData, (req, res) => {
  const { email, password } = req.body;
    // Vérifier qu'un compte existe bien avec l'email fourni.
    connection.execute(
      `SELECT * FROM user WHERE email=?`,
      [email],
      (mysqlError, result) => {
        if (mysqlError) {
          res.status(500).json({ error: mysqlError });
        } else if (result.length === 0) {
          res.status(401).json({ error: "Invalid email" });
        } else {
          const user = result[0];
          // Récupérer le mot de passe haché en base de données.
          const hashedPassword = user.password;
          // Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.
          bcrypt.compare(
            password,
            hashedPassword,
            function (bcryptError, passwordMatch) {
              if (bcryptError) {
                res.status(500).json({ error: bcryptError });
              } else if (passwordMatch) {
                // passwordMatch est vrai si le mot de passe correspond.
                // Retourner le compte connecté (mais sans le mot de passe !).
                res.status(200).json({
                  id: user.id,
                  email: user.email,
                });
              } else {
                res.status(401).json({ error: "Invalid password" });
              }
            }
          );
        }
      }
    );
  }
);
module.exports = router;