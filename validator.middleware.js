// validator.middleware.js voiture
const validatecarData = (req, res, next) => {
  const { brand, model } = req.body;

  // Vérifier que la marque et le model sont présents dans la requête
  if (!brand || !model) {
    return res
      .status(400)
      .json({ message: "La marque et le modèle sont obligatoires." });
  }
  next();
};

// validator.middleware.js garage
const validategarageData = (req, res, next) => {
  const { name, email } = req.body;

  // Vérifier que le nom et l'email sont présents dans la requête
  if (!name || !email) {
    return res
      .status(400)
      .json({ message: "Le nom et l'email sont obligatoires." });
  }

  // Vérifier que l'email a un format valide
  if (!validateEmail(email)) {
    return res
      .status(400)
      .json({ message: "L'email n'a pas un format valide." });
  }
  next();
};

const validateUserData = (req, res, next) => {
  const { email, password } = req.body;
    if (!email || !password) {
      return res
        .status(400)
        .json({ message: "Le nom et l'email sont obligatoires." });
  }
    if (!validateEmail(email)) {
      return res
        .status(400)
        .json({ message: "L'email n'a pas un format valide." });
  }
  next();
}  

// Fonction de validation de l'email
const validateEmail = (email) => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+/; // Ajout de la définition de emailRegex
  return emailRegex.test(email);
};



module.exports = {
  validatecarData,
  validategarageData,
  validateUserData,
};
